import { dbQuery, syncDB } from "./utils/db";
import { get } from "request-promise-native";
import { random } from "faker";
import { exponentialBuckets } from "prom-client";
import { expect } from "chai";

const POSITION_HOST = process.env['POSITION_HOST'] || 'localhost';
const POSITION_PORT = process.env['POSITION_PORT'] || 3001;


describe("Position Server", function () {
    const id = 4;
    this.timeout(30000);

    before(function (done) {
        setTimeout(done, 15000);
    });

    beforeEach(async function () {
        await syncDB();
        // reset database
        await dbQuery({
            type: "remove",
            table: "driver_position"
        });
        // input data dummy
        this.returnPos = (await dbQuery({
            type: "insert",
            table: "driver_position",
            values: {
                "rider_id": id,
                "latitude": 10,
                "longitude": 20,
                "createdAt": (new Date()),
                "updatedAt": new Date()
            },
            returning: [
                "rider_id",
                "latitude",
                "longitude",
                "createdAt"
            ]
        }))[0][0];
    });

    describe("Postition", function () {
        it("harusnya memberikan position suatu driver", async function () {
            const res = await get(
                `http://${POSITION_HOST}:${POSITION_PORT}/position/${id}`
            );
            console.log(res[0]);
            const obj = {
                latitude: this.returnPos.latitdue,
                longitude: this.returnPos.longitude
            }
            expect(res[0]).to.be.deep.eq(obj);
        });
    });
});