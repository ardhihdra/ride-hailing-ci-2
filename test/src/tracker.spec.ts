import { dbQuery, syncDB } from "./utils/db";
import { get } from "request-promise-native";
import { random } from "faker";
import { exponentialBuckets } from "prom-client";
import { expect } from "chai";

const TRACKER_HOST = process.env['TRACKER_HOST'] || 'localhost';
const TRACKER_PORT = process.env['TRACKER_PORT'] || 3000;
const POSITION_HOST = process.env['POSITION_HOST'] || 'localhost';
const POSITION_PORT = process.env['POSITION_PORT'] || 3001;


describe("Tracker Server", function () {
    const id = 4;
    this.timeout(30000);

    before(function (done) {
        setTimeout(done, 15000);
    });

    beforeEach(async function () {
        await syncDB();
        // reset database
        await dbQuery({
            type: "remove",
            table: "track_events"
        });
        // input data dummy
        this.returning = (await dbQuery({
            type: "insert",
            table: "track_events",
            values: {
                "rider_id": id,
                "north": random.number({ min: 1, max: 20 }),
                "south": random.number({ min: 1, max: 20 }),
                "east": random.number({ min: 1, max: 20 }),
                "west": random.number({ min: 1, max: 20 }),
                "createdAt": (new Date()),
                "updatedAt": new Date()
            },
            returning: [
                "rider_id",
                "north",
                "south",
                "west",
                "east",
                "createdAt"
            ]
        }))[0][0];
    });
    // this.returPos = (await dbQuery({
    //     type: "insert",
    //     table: "driver_position",
    //     values: {
    //         "rider_id": id,
    //         "latitude": 10,
    //         "longitude": 20,
    //         "createdAt": (new Date()),
    //         "updatedAt": new Date()
    //     },
    //     returning: [
    //         "rider_id",
    //         "latitude",
    //         "longitude",
    //         "createdAt"
    //     ]
    // }))[0][0];
    describe("Movement", function () {
        it("harusnya memberikan data movement suatu driver", async function () {
            const response = await get(
                `http://${TRACKER_HOST}:${TRACKER_PORT}/movement/${id}`, { json: true }
            );
            console.log(response.logs[0])
            const obj = {
                east: this.returning.east,
                south: this.returning.south,
                north: this.returning.north,
                west: this.returning.west,
                time: this.returning.createdAt.toISOString()
            }
            expect(response.logs[0]).to.be.deep.eq(obj);
        });
    });

    // describe("Postition", function () {
    //     it("harusnya memberikan position suatu driver", async function () {
    //         const res = await get(
    //             `http://${POSITION_HOST}:${POSITION_PORT}/position/${id}`
    //         );
    //         console.log(res[0]);
    //             latitude: this.returning.latitdue,
    //         const obj = {
    //             longitude: this.returning.longitude
    //         }
    //         expect(res[0]).to.be.deep.eq(obj);
    //     })
    // });
});